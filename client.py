import logging
import sys
from pprint import pprint

from decouple import config
from tenable.io import TenableIO as tio


# Set logging level for debugging purposes
logging.basicConfig(level=logging.DEBUG)

# Instantiate the client
client = tio(access_key=config('ACCESS_KEY'), secret_key=config('SECRET_KEY'))

vulns = client.exports.vulns(severity=['high', 'critical'])
# Get length of iterator 
print(len(list(vulns)))
